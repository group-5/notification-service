const express = require('express')
const router = express.Router()
const Notification = require('../schema/schema')
const jwt = require("jwt-simple");
const mongoose = require('mongoose');
// Recieve the notifaction 
// user id, title, description
// send these items to the mongodb

// whenever notifcation page is called, send the notifications for that user
// using the user id

// if the user wants to delete the notifcations
// get that repsond - delete the notifcations from the database

const jwtAlgorithm = "HS512";


// retrieve all the notifications for a user that are stored in the database.
router.get("/info", async (req, res) => {
  console.log("fetching notifications called")
  try {

    // Get the JWT secret from the environment variables
    const secretKey = process.env.JWT_SECRET;

    // If this is not set we want to throw an error as this is required to retrieve the user
    // id from the provided token.
    if (secretKey == null) {
      console.error('JWT_SECRET is not set in the environment variables');
      return res.status(500).send("JWT_SECRET is not set in the environment variables");
    }

    // Get the token from the request headers
    const token = req.headers.authorization.split(" ")[1];

    // Decode this token with the secret key
    const payload = jwt.decode(token, secretKey, false, jwtAlgorithm);

    // Get the user id from the decoded token payload.
    const userId = payload.id;

    console.log("fetching the notifications for user: " + userId)

    // Find all the notifications for the user and return them without the user_id and __v fields.
    // as this should not be exposed to the user.
    const notifications = await Notification.find({ user_id: userId }, { user_id: 0, __v: 0 });

    console.log("found notifications for user: " + userId + " " + notifications.length + " notifications found.")

    res.status(200).send(notifications);

  } catch (error) {
    res.status(500).send(error);
  }
});


router.delete("/delete/:id", async (req, res) => {
  // delete a notification
  // grab the notification id
  // delete on the database
  console.log('delete notification called')
  try {

    if (req.params.id == null) {
      console.error('notification id is null');
      return res.status(400).send();
    }

    console.log('finding and deleting notification with id = ' + req.params.id)

    const notifications = await Notification.findByIdAndDelete(req.params.id)

    if (!notifications) {
      console.error('notification not found')
      return res.status(500).send();
    }

    return res.status(200).send();
  } catch (error) {
    console.error('error was thrown when deleting notification with id = ' + req.params.id)
    return res.status(500).send(error);
  }

});

router.delete("/delete-all", async (req, res) => {
  console.log('delete all notifications called')
  // delete all the notifications
  // grab the user id
  // delete on the database
  try {
    // Get the JWT secret from the environment variables
    const secretKey = process.env.JWT_SECRET;

    // If this is not set we want to throw an error as this is required to retrieve the user
    // id from the provided token.
    if (secretKey == null) {
      console.error('JWT_SECRET is not set in the environment variables');
      return res.status(500).send("JWT_SECRET is not set in the environment variables");
    }

    // Get the token from the request headers
    const token = req.headers.authorization.split(" ")[1];

    // Decode this token with the secret key
    const payload = jwt.decode(token, secretKey, false, jwtAlgorithm);

    // Get the user id from the decoded token payload.
    const userId = payload.id;

    console.log("deleting all notifications for user: " + userId)

    const notifications = await Notification.deleteMany({ user_id: userId })


    // If the delete was not successful send a 500 error.
    if (notifications.acknowledged != true) {
      return res.status(500).send();
    }

    return res.status(200).send();
  } catch (error) {
    console.error('error was thrown when deleting all notifications for user: ' + userId + 'error = ' + error)
    return res.status(500).send(error);
  }

});



router.post("/create", async (req, res) => {
  //  receive the notification info from the user
  // send the notification to the database (create/save/insert methods)

  const date = new Date;
  const notifications = new Notification({
    user_id: req.body['user_id'],
    title: req.body['title'],
    description: req.body['description'],
    date: date
  });

  try {
    await notifications.save();
    res.status(201).send(notifications);
  } catch (error) {
    res.status(400).send(error);
  }

});

module.exports = router