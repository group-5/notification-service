const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
    user_id: String,
    title: String,
    description: String,
    date: Date
});

const Notification = mongoose.model('Notification',notificationSchema);
module.exports = Notification;