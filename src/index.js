// import { MongoClient } from "mongodb";
const database = require('./db/mongoose');
require('dotenv').config();

const cors = require('cors');
const express = require('express');
const notificationRouter = require("./routes/notification");

const app = express();
const port = process.env.PORT;
app.use(express.json());
app.use(cors());


(async () => {

  console.log('Setting up database...');
  await database()


  app.use("/notification", notificationRouter);

  app.listen(port, function () {
    console.log(`Notification Microservice is running at http://localhost:${port}`);
  })
})()

